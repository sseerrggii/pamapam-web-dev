<!--ts-->
   * [Linux](#linux)
      * [Docker](#docker)
         * [Instalar prerequisitos](#instalar-prerequisitos)
         * [Instalar docker](#instalar-docker)
         * [Clonar frontend-dev repository](#clonar-frontend-dev-repository)
         * [Create dockers](#create-dockers)
         * [Listo!](#listo)
      * [LAMP](#lamp)
         * [Instalar prerequisitos](#instalar-prerequisitos-1)
         * [Clone frontend-dev repository](#clone-frontend-dev-repository)
         * [Clone frontend](#clone-frontend)
         * [Crear e importar database](#crear-e-importar-database)
         * [Configurar Apache](#configurar-apache)
         * [Habilitar el idioma por defecto](#habilitar-el-idioma-por-defecto)
         * [Listo!](#listo-1)
   * [Windows](#windows)
      * [Docker](#docker-1)
      * [WAMP](#wamp)
         * [Instalar Sublime Text editor](#instalar-sublime-text-editor)
         * [Instalar Git for Windows](#instalar-git-for-windows)
         * [Clone frontend-dev repository](#clone-frontend-dev-repository-1)
         * [Instalar WAMP server](#instalar-wamp-server)
         * [Start WAMP server.](#start-wamp-server)
         * [Clone frontend repository](#clone-frontend-repository)
         * [Crear virtual server](#crear-virtual-server)
         * [Habilitar proxy module](#habilitar-proxy-module)
         * [Configurar virtual server](#configurar-virtual-server)
         * [Importar base de datos de desarrollo](#importar-base-de-datos-de-desarrollo)
         * [Habilitar el idioma por defecto](#habilitar-el-idioma-por-defecto-1)
         * [Listo!](#listo-2)

<!-- Added by: martin, at: 2019-01-02T01:54+01:00 -->

<!--te-->

Instrucciones para la instalación del entorno de desarrollo del frontend de la web de Pam a Pam. Tanto para Linux como para Windows tienes 2 opciones, o bien con Docker (la opción más sencilla) o bien instalando los recursos necesarios en nuestro pc.   

# Linux

## Opoción 1: Docker

### Instalar prerequisitos

```bash
sudo apt-get install git git-lfs
```

### Instalar docker

```bash
wget -qO- https://get.docker.com/ | sh
sudo usermod -aG docker $USER
```

restart

### Clonar frontend-dev repository

```bash
mkdir ~/pamapam
git lfs clone https://gitlab.com/jamgo/pamapam-web-dev.git ~/pamapam/pamapam-web-dev
```

### Create dockers

```bash
cd ~/pamapam/pamapam-web-dev/linux/docker
./create.sh
```

### Listo!

Para acceder normalmente al site local de desarrollo:

http://pamapam.local

Admin page de Wordpress:

http://pamapam.local/wp-admin

User: ```_admin_```  
Password: ```pamapam```  

## Opción 2: LAMP

### Instalar prerequisitos

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install git git-lfs apache2 php7.1 mariadb-server php7.1-fpm php7.1-dev php7.1-zip php7.1-curl php7.1-xmlrpc php7.1-gd php7.1-mysql php7.1-mbstring php7.1-xml libapache2-mod-php7.1
```

### Clone frontend-dev repository

```bash
mkdir ~/pamapam
git lfs clone https://gitlab.com/jamgo/pamapam-web-dev.git ~/pamapam/pamapam-web-dev
```

### Clone frontend

```bash
git clone https://gitlab.com/pamapam/frontend.git ~/pamapam/pamapam-web
sudo ln -sv ~/pamapam/pamapam-web /var/www/pamapam-web
cp ~/pamapam/pamapam-web-dev/resources/lamp/wp-config.php ~/pamapam/pamapam-web
unzip ~/pamapam/pamapam-web-dev/resources/uploads.zip -d ~/pamapam/pamapam-web/wp-content
```

### Crear e importar database

```bash
sudo mysql -u root -e "create user pamapam@'%' identified by 'pamapam';"
sudo mysql -u root -e "create database pamapam_wp character set utf8 collate utf8_general_ci;"
sudo mysql -u root -e "grant all on pamapam_wp.* to pamapam@'%' identified by 'pamapam';"
mysql -u pamapam -ppamapam < ~/pamapam/pamapam-web-dev/resources/pamapam_wp_development.sql
```

### Configurar Apache

```bash
sudo cp ~/pamapam/pamapam-web-dev/resources/lamp/pamapam-web.conf /etc/apache2/sites-available/
sudo -- sh -c "echo '127.0.0.1 pamapam.local' >> /etc/hosts"
sudo a2enmod proxy_http
sudo a2ensite pamapam-web
sudo systemctl restart apache2
```

### Habilitar el idioma por defecto

Antes de poder entrar por primera vez a la página de inicio de la web Pam a Pam en lo instalación local, hay que actualizar el idioma por defecto desde el panel de administración del Wordpress:

http://pamapam.local/wp-admin

User: ```_admin_```  
Password: ```pamapam```  

```
Idiomas -> Catalán -> Actualizar
```

### Listo!

Para acceder normalmente al site local de desarrollo:

http://pamapam.local

ó

```
wampserver icon -> left click -> Your VirtualHosts -> pamapam.local
```

# Windows

## Opción 1: Docker

*TO DO*

## Opción 2 WAMP

### Instalar Sublime Text editor

https://www.sublimetext.com/

### Instalar Git for Windows

https://gitforwindows.org/

Elegir Sublimetext como editor. Hay un error en el instalador y aún estando instalado el Sublimetext no habilita el botón Next. Hay que hacer Back y Next para poder continuar...

### Clone frontend-dev repository

Open Git Bash

```bash
mkdir ~/pamapam
cd pamapam
git clone https://gitlab.com/jamgo/pamapam-web-dev.git
```

### Instalar WAMP server

Instalar primero todos los Visual C++ Packages. Si es un windows 64 bits hay que instalar también todos los packages 32 bits. Instalarlos en el orden en que aparecen en las listas.

http://wampserver.aviatechno.net/?lang=en#vcpackages

Instalar la última full version (3.1.4 en este momento). Elegir Sublime Text editor como editor por defecto.

http://wampserver.aviatechno.net/

### Start WAMP server. 

Esperar a que el ícono se ponga en verde.

### Clone frontend repository

Open Git Bash

```bash
git clone https://gitlab.com/pamapam/frontend.git /c/wamp64/www/pamapam-web
cp ~/pamapam/pamapam-web-dev/resources/wamp/wp-config.php /c/wamp64/www/pamapam-web
unzip ~/pamapam/pamapam-web-dev/resources/uploads.zip -d /c/wamp64/www/pamapam-web/wp-content
```

### Crear virtual server

```
wampserver icon -> left click -> Your VirtualHosts -> VirtualHost Management
```

Name of the Virtual Host: **pamapam.local**  
Complete absolute path of the VirtualHost folder: **c:/wamp64/www/pamapam-web**  

```
wampserver icon -> right click -> Tools -> Restart DNS
wampserver icon -> left click -> PHP -> Version -> 7.1.xx
wampserver icon -> right click -> Tools -> Change PHP CLI version -> 7.1.xx
```

### Habilitar proxy module

Para poder conectarse al backoffice de QA hay que configurar el apache para que haga las redirecciones adecuadas.

```
wampserver icon -> left click -> Apache -> Apache modules -> proxy module
wampserver icon -> left click -> Apache -> Apache modules -> proxy http module
```

### Configurar virtual server

```
wampserver icon -> left click -> Apache -> httpd-vhosts.conf
```

Cambiar:

```apache
<VirtualHost *:80>
	ServerName pamapam.local
	DocumentRoot "c:/wamp64/www/pamapam-web"
	<Directory  "c:/wamp64/www/pamapam-web/">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require local
	</Directory>
</VirtualHost>
```

por:

```apache
<VirtualHost *:80>
	ServerName pamapam.local
	DocumentRoot "c:/wamp64/www/pamapam-web"
	<Directory  "c:/wamp64/www/pamapam-web/">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require local
	</Directory>
	ProxyPass "/backoffice" "http://pamapam-qa.jamgo.org/backoffice"
	ProxyPassReverse "/backoffice" "http://pamapam-qa.jamgo.org/backoffice"
	ProxyPass "/backoffice/" "http://pamapam-qa.jamgo.org/backoffice/"
	ProxyPassReverse "/backoffice/" "http://pamapam-qa.jamgo.org/backoffice/"
	ProxyPass "/services/" "http://pamapam-qa.jamgo.org/services/"
	ProxyPassReverse "/services/" "http://pamapam-qa.jamgo.org/services/"
</VirtualHost>
```

```
wampserver icon -> left click -> Apache -> Service administration -> Restart Service
```

### Importar base de datos de desarrollo

```
wampserver icon -> left click -> phpMyAdmin
```

User: ```root```  
Password: *vacia*  

```
Importar -> Browse
```

Seleccionar: ~/pamapam/resources/pamapam_wp_development.sql

Iniciar la importación.

### Habilitar el idioma por defecto

Antes de poder entrar por primera vez a la página de inicio de la web Pam a Pam en lo instalación local, hay que actualizar el idioma por defecto desde el panel de administración del Wordpress:

http://pamapam.local/wp-admin

User: ```_admin_```  
Password: ```pamapam```  

```
Idiomas -> Catalán -> Actualizar
```

### Listo!

Para acceder normalmente al site local de desarrollo:

http://pamapam.local

ó

```
wampserver icon -> left click -> Your VirtualHosts -> pamapam.local
```
